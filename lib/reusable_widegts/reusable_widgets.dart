import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_demo/services/firebase_services.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../screens/recipe_list_screen.dart';

Image LogoWidget(String imageName) {
  return Image.asset(imageName, fit: BoxFit.fitWidth,
    width: 200,
    height: 200,
    color: Colors.white,
  );
}

TextField resuableTextField( String text, IconData icon,bool isPasswordType,TextEditingController controller
){
return TextField(
  controller: controller,
  obscureText: isPasswordType,
  enableSuggestions: !isPasswordType,
  autocorrect: !isPasswordType,
  cursorColor: Colors.white,
  style: TextStyle(color: Colors.white.withOpacity(0.9)),
  decoration: InputDecoration(prefixIcon: Icon(icon,color: Colors.white70,
  ),
    labelText: text,
    labelStyle: TextStyle(color: Colors.white.withOpacity(0.9)),
    filled: true,
    floatingLabelBehavior: FloatingLabelBehavior.never,fillColor: Colors.white.withOpacity(0.3),
    border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0),borderSide: BorderSide(width: 0,style: BorderStyle.none)
    )
  ),
  keyboardType: isPasswordType ? TextInputType.visiblePassword: TextInputType.emailAddress,
);

}

Container signInSignUpButton(
BuildContext context,bool isLogin,onTap) {
  return Container(
    width: MediaQuery
        .of(context)
        .size
        .width,
    height: 50,
    margin: const EdgeInsets.fromLTRB(0, 10, 0, 20),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(90)),
    child: ElevatedButton(
      onPressed: () {
        onTap();
      },
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith((states) {
          if (states.contains(MaterialState.pressed)) {
            return Colors.black26;
          }

          return Colors.white;
        }
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30))
        ),


      ),
      child: Text(
        isLogin ? 'LOG IN' : 'SIGN UP',
        style: const TextStyle(
            color: Colors.black87, fontWeight: FontWeight.bold, fontSize: 16),
      ),
    ),
  );
}

  Container googleSignIn(BuildContext context, bool isLogin, onTap) {
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      height: 50,
      margin: const EdgeInsets.fromLTRB(0, 10, 0, 20),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(90)),
      child: ElevatedButton(
        onPressed: () async {
          final response =
          await FirebaseServices().signInWithGoogele();
          if(FirebaseAuth.instance.currentUser?.email != null){
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            await sharedPreferences.setString('name', FirebaseAuth.instance.currentUser?.displayName ?? "");
            await sharedPreferences.setString('email', FirebaseAuth.instance.currentUser?.email ?? "");
            await sharedPreferences.setString('photo', FirebaseAuth.instance.currentUser?.photoURL! ?? "");
            Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const RecipeListScreen()));
          } else{
            print('try to sign in');
          }

          onTap();
        },
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.pressed)) {
              return Colors.black26;
            }

            return Colors.white;
          }
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30))
          ),


        ),
        child : Padding(
          padding: const EdgeInsets.all(8.0),
          child : Padding(
            padding: const EdgeInsets.all(8.0),

          child: Row(
            children: <Widget>[
              const SizedBox(width: 30),
              Image.asset("assets/images/google_login_img.png",width: 40,height: 40,),
               const SizedBox(width: 16),
               Text(
                isLogin ? 'Google Login' : '',
                style:  const TextStyle(
                    color: Colors.black87, fontWeight: FontWeight.bold, fontSize: 16),

              ),

            ],
          ),
          ),
        ),

      ),
    );
  }

