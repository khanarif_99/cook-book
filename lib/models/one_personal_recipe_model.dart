class OnePersonalRecipeModel {
  bool? success;
  List<OnePersonalRecipe>? recipes;

  OnePersonalRecipeModel({success, recipes});

  OnePersonalRecipeModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['recipes'] != null) {
      recipes = <OnePersonalRecipe>[];
      json['recipes'].forEach((v) {
        recipes!.add(OnePersonalRecipe.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['success'] = success;
    if (recipes != null) {
      data['recipes'] = recipes!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OnePersonalRecipe {
  String? sId;
  String? name;
  String? description;
  int? kCal;
  List<String>? directions;
  List<Ingredients>? ingredients;
  String? isPersonal;
  String? createdAt;
  String? updatedAt;
  int? iV;

  OnePersonalRecipe(
      {sId,
        name,
        description,
        kCal,
        directions,
        ingredients,
        isPersonal,
        createdAt,
        updatedAt,
        iV});

  OnePersonalRecipe.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
    description = json['description'];
    kCal = json['kCal'];
    directions = json['directions'].cast<String>();
    if (json['ingredients'] != null) {
      ingredients = <Ingredients>[];
      json['ingredients'].forEach((v) {
        ingredients!.add(Ingredients.fromJson(v));
      });
    }
    isPersonal = json['isPersonal'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    data['name'] = name;
    data['description'] = description;
    data['kCal'] = kCal;
    data['directions'] = directions;
    if (ingredients != null) {
      data['ingredients'] = ingredients!.map((v) => v.toJson()).toList();
    }
    data['isPersonal'] = isPersonal;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = iV;
    return data;
  }
}

class Ingredients {
  String? name;
  String? quantity;
  String? sId;

  Ingredients({name, quantity, sId});

  Ingredients.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    quantity = json['quantity'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['name'] = name;
    data['quantity'] = quantity;
    data['_id'] = sId;
    return data;
  }
}