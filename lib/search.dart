import 'package:firebase_demo/models/personal_recipe_model.dart';
import 'package:flutter/material.dart';


class SearchUser extends SearchDelegate {
  List<PersonalRecipes> recipeList;

  SearchUser({required this.recipeList});

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: const Icon(Icons.close))
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back_ios),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    recipeList=recipeList.where((element) => element.name!.contains(query)).toList();
    return ListView.builder(
        itemCount: recipeList.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Row(
                    children: [
                      const SizedBox(
                        width: 60,
                        height: 60,

                          child: CircleAvatar(
                            radius: 60,
                            backgroundImage: AssetImage(
                                'assets/images/chicken_pic.jpg'),
                          )
                        ),

                      const SizedBox(width: 20),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '${recipeList[index].name}',
                              style: const TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(height: 10),
                            Text(
                              '${recipeList[index].description}',
                              style: const TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ])
                    ],
                  ),
                );
              });

  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Center(
      child: TextField(onChanged: (String val){
        recipeList=recipeList.where((element) => element.name!.contains(val)).toList();
      }),
    );
  }
}