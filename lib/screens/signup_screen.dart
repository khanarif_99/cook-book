import 'dart:convert';

import 'package:firebase_demo/reusable_widegts/reusable_widgets.dart';
import 'package:firebase_demo/screens/signin_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import '../utils/color_utils.dart';
class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
   TextEditingController _userNameTextController = TextEditingController();
  TextEditingController _emailTextController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

   void successfulSignUpMessage(BuildContext context){
     const snackBar = SnackBar(
       content: Text('Account created Successfully.'),
     );

// Find the ScaffoldMessenger in the widget tree
// and use it to show a SnackBar.
     ScaffoldMessenger.of(context).showSnackBar(snackBar);
   }
   void emptySignUpMessage(BuildContext context){
     const snackBar = SnackBar(
       content: Text('Please Enter Details'),
     );

// Find the ScaffoldMessenger in the widget tree
// and use it to show a SnackBar.
     ScaffoldMessenger.of(context).showSnackBar(snackBar);
   }

  void signUp(String name,email,password) async{
  try{
      Response response =  await post(
    Uri.parse('https://recipeappserver.herokuapp.com/register'),

    body: {
      'email':email,
      'password': password,
      'name' : name
    }
      );

      if(response.statusCode == 200){
        var data = jsonDecode(response.body.toString());
        print(data);
      print('Account created Successfully');
      } else {
        print(response.body.toString());
        
      }
  } catch(e) {
    print(e.toString());
  }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: const Text('Sign Up',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 24),

        ),

      ),
        body: Container(
        width: MediaQuery
        .of(context)
        .size
        .width,
    height: MediaQuery
        .of(context)
        .size
        .height,
    decoration: BoxDecoration(gradient: LinearGradient(colors: [
    hexStringToColor("CB2B93"),
    hexStringToColor("9546C4"),
    hexStringToColor("5E61F4")
    ],
    begin: Alignment.topCenter, end: Alignment.bottomCenter
    )
    ),

    child: SingleChildScrollView(
    child: Padding(
    padding: EdgeInsets.fromLTRB(20, MediaQuery
        .of(context)
        .size
        .height * 0.2, 20, 0
    ),

      child: Column(
        children: <Widget>[
          const SizedBox(height: 20,
          ),
          resuableTextField('Enter UserName', Icons.person_outline, false, _userNameTextController),
          const SizedBox(height: 20,
          ),
          resuableTextField('Enter Email', Icons.email_outlined, false, _emailTextController),

          const SizedBox(height: 20,
          ),
          resuableTextField('Enter Password', Icons.lock_outline, true, _passwordController),
          const SizedBox(height: 20,
          ),
          signInSignUpButton(context, false, (){
            // FirebaseAuth.instance.createUserWithEmailAndPassword(email: _emailTextController.text, password: _passwordController.text).then((value){
            //   print("Create new Account");
            //   Navigator.push(context, MaterialPageRoute(builder:(context) => const HomeScreen()));
            // }).onError((error, stackTrace) {
            //   print("error ${error.toString()}'");
            // });
            if(_userNameTextController.text.toString().isNotEmpty && _emailTextController.text.toString().isNotEmpty && _passwordController.text.toString().isNotEmpty){
              signUp(_userNameTextController.text.toString(), _emailTextController.text,_passwordController.text.toString() );
              successfulSignUpMessage(context);
              Navigator.push(context, MaterialPageRoute(builder: (context) => const SignInScreen()));
            } else {
              emptySignUpMessage(context);
            }

          })


        ],
      ),
    )
    ),
    )
    );
  }
}
