import 'package:flutter/material.dart';

import '../models/recipe_model.dart';

class RecipesDetailList extends StatelessWidget {
  final double backPhotoHeight = 200;
  final Recipes recipe;
  const RecipesDetailList({Key? key,required this.recipe}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.blue,
        body: Stack(
          children: [
             const Positioned(
               top: 0,
               right:-10,
               child: CircleAvatar(
                 radius: 60,
                 backgroundImage: AssetImage('assets/images/chicken_pic.jpg'),

               ),


             ),
             GestureDetector(
               onTap: (){
                 Navigator.pop(context);
               },
                 child: backGroundImage()),
            Expanded(
              child: Container(

               margin: const EdgeInsets.only(top: 140),
                padding: const EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30.0),
                ),

                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                       Text(recipe.name.toString(),
                        style: const TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black87),
                      ),
                        Container(
                            padding: const EdgeInsets.all(16.0),
                            decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child:  Text('${recipe.kCal.toString()} Calories',
                              style: const TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white),)),


                      ],
                    ),
                    const SizedBox(height: 10),

                     Text(recipe.directions.toString(),
                      style: const TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.normal,
                          color: Colors.grey),
                    ),

                    const SizedBox(height:30),
                    const Text('Ingredients:',
                      style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black87),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 16),
                      padding: const EdgeInsets.all(20.0),
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                      child: Row(
                        children:  [
                          const CircleAvatar(
                            backgroundImage: AssetImage('assets/images/chicken_pic.jpg'),
                          ),
                          const SizedBox(width: 10),
                          Text(
                           recipe.ingredients![0].name.toString(),
                            style: const TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.grey),
                          ),
                          const SizedBox(width: 70),

                          Text('${recipe.ingredients![0].quantity} Calories',
                            style: const TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.grey),
                          ),


                        ],
                      ),
                    ),
                    const SizedBox(height: 16),
                    const Text('Directions',
                      style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black87),
                    ),
                    const SizedBox(height: 20),

                    Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: 10.0,
                          height: 10.0,
                          decoration: const BoxDecoration(
                            color: Colors.blue,
                            shape: BoxShape.circle,
                          ),
                        ),
                        Text(recipe.directions.toString(),
                          style: const TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.normal,
                              color: Colors.grey),
                        )

                      ],
                    ),


                  ],

                ),

              ),
            ),
          ],


        ),




        ),


    );

  }
  Widget backGroundImage() => Container(
    margin: const EdgeInsets.symmetric(horizontal: 4, vertical: 15),
    padding: const EdgeInsets.all(10.0),
    child:  const Icon(Icons.arrow_back,size: 30,color: Colors.white,
    ),

  );



}




