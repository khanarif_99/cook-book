import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:firebase_demo/screens/signin_screen.dart';
import 'package:flutter/material.dart';
class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CookBook',
      home: AnimatedSplashScreen(
          splash: Image.asset('assets/images/recipe_pic.jpg'),
          duration: 3000,
          splashTransition: SplashTransition.fadeTransition,
          backgroundColor: Colors.blueGrey,
          nextScreen: SignInScreen()),
    );
  }
}
