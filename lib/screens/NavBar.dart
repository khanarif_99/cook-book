



import 'package:firebase_demo/screens/personal_recipe_screen.dart';
import 'package:firebase_demo/screens/signin_screen.dart';
import 'package:firebase_demo/services/firebase_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NavBar extends StatefulWidget {
  const NavBar({Key? key}) : super(key: key);



  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  late SharedPreferences prefs;
  String userName = '';
  String email = '';
  String? userPhoto = '';
  @override
  void initState() {
    super.initState();
    retrieveStringValue();
  }
  retrieveStringValue  () async
  {
    Future.delayed(const Duration(milliseconds: 500),() async{
      prefs =  await  SharedPreferences.getInstance() ;
      email =  prefs.getString("email")!;
      userName =  prefs.getString("name")!;
      userPhoto = prefs.getString("photo")??'';
      setState(() {


      });
    });



  }


  @override
  Widget build(BuildContext context) {

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(accountName:userName!=''?Text(userName.toString(),style: TextStyle(color: Colors.white70),):const CupertinoActivityIndicator() , accountEmail: email!=''?Text(email.toString(),style: TextStyle(color: Colors.white70)):const CupertinoActivityIndicator(),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: userPhoto == ''  ? Image.asset('assets/images/profile_pic.png',fit: BoxFit.cover,width: 90,height: 90,)
                    :Image.network(
                  userPhoto!,fit: BoxFit.cover,
                ) ,
              ),
            ),
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(
                      'assets/images/forest_pic.jpg',
                    ),
                    fit: BoxFit.cover
                )
            ),


          ),

          ListTile(
            leading: const Icon(Icons.favorite),
            title: const Text('My Personal Recipes'),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => const PersonalRecipeScreen()));
            },
          ),
          ListTile(
              leading: const Icon(Icons.exit_to_app),
              title: const Text('Log Out'),
              onTap: () async {
                await FirebaseServices().signOut();
                prefs.clear();
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) =>const SignInScreen()));

              }


          ),
        ],
      ),

    );
  }


}
