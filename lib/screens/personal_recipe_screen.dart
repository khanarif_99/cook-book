import 'dart:convert';
import 'dart:developer';
import 'package:firebase_demo/search.dart';
import 'package:firebase_demo/utils/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_demo/models/personal_recipe_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class PersonalRecipeScreen extends StatefulWidget {
  const PersonalRecipeScreen({Key? key}) : super(key: key);


  @override
  State<PersonalRecipeScreen> createState() => _PersonalRecipeScreenState();
}

class _PersonalRecipeScreenState extends State<PersonalRecipeScreen> {
  late  List<PersonalRecipes> recipeList=[];
  Future<List<PersonalRecipes>> fetchPost() async {
    final response = await http.get(
        Uri.parse(
            'https://recipeappserver.herokuapp.com/getAllPersonalRecipes'),
        headers: {'Authorization': Constants.token});

    if (response.statusCode == 200) {
      final parsed = json.decode(response.body);

      PersonalRecipeModel personalRecipeModel =
          PersonalRecipeModel.fromJson(parsed);
      for (var element in personalRecipeModel.recipes!) {
        debugPrint(element.name);
      }

      print('myPersonalRecipes: $personalRecipeModel');
      recipeList= personalRecipeModel.recipes!;
      return personalRecipeModel.recipes!;
    } else {
      throw Exception('Failed to load personal recipes');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent,
        title: const Text(
          'My Personal Recipes',
          style: TextStyle(color: Colors.white),
        ),
        actions:  [
          IconButton(icon: Icon(Icons.search),
          onPressed: (){
         showSearch(context: context, delegate: SearchUser(recipeList:recipeList));
            },)


        ],
      ),
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.circular(20.0),
        ),
        margin: const EdgeInsets.fromLTRB(14, 20, 0, 0),
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          // just loading
          children: [
            const SizedBox(height: 20),
            const Text(
              'Available Recipes',
              style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black87),
            ),
            const SizedBox(height: 10),
            const Text(
              'Enjoy one of our delicious fruit \nrecipes',
              style: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.normal,
                color: Colors.grey,
              ),
            ),
            FutureBuilder<List<PersonalRecipes>>(
              future: fetchPost(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return CarouselSlider.builder(
                      options: CarouselOptions(
                        aspectRatio: 1.0,
                        enlargeCenterPage: true,
                        enableInfiniteScroll: false,
                        initialPage: 2,
                        autoPlay: true,
                      ),
                      itemCount: snapshot.data!.length,
                      itemBuilder: (_, index, pageIndex) {
                        final PersonalRecipes recipe = snapshot.data![index];
                        print(recipe.name.toString());

                        return GestureDetector(
                          onTap: () {
                            // Navigator.push(context, MaterialPageRoute(builder: (context)=>const RecipesDetailList()));
                            // Navigator.of(context).push(MaterialPageRoute(builder: (context) =>RecipesDetailList(recipe:recipe)));
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SizedBox(
                              width: 250,
                              child: Stack(
                                children: [
                                  Container(
                                    height: 400,
                                    width: 200,
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 15),
                                    padding: const EdgeInsets.all(20.0),
                                    decoration: BoxDecoration(
                                      color: const Color(0xff97FFFF),
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          recipe.name.toString(),
                                          style: const TextStyle(
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const SizedBox(height: 10),
                                        Text(
                                          recipe.description.toString(),
                                          style: const TextStyle(
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white),
                                        ),
                                        const SizedBox(height: 10),
                                        Text(
                                          recipe.kCal.toString(),
                                          style: const TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white),
                                        ),
                                        const SizedBox(height: 20),
                                      ],
                                    ),
                                  ),
                                  const Positioned(
                                    top: 0,
                                    right: 0,
                                    child: CircleAvatar(
                                      radius: 70,
                                      backgroundImage: AssetImage(
                                          'assets/images/chicken_pic.jpg'),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                } // if condition closed here

                return const Center(child: CircularProgressIndicator());
              },
            ),
          ],
        ),
      ),
    );
  }
}
