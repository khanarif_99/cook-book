import 'dart:convert';
import 'package:firebase_demo/screens/signup_screen.dart';
import 'package:firebase_demo/utils/color_utils.dart';
import 'package:flutter/material.dart';
import 'package:firebase_demo/reusable_widegts/reusable_widgets.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../reusable_widegts/reusable_widgets.dart';
import 'recipe_list_screen.dart';
class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  late bool isLoading;
  static TextEditingController _passwordTextController = TextEditingController();
  TextEditingController _emailTextController = TextEditingController();






  //instance of getStorage
  void logIn(String email,password) async{
    try{
      Response response =  await post(
          Uri.parse('https://recipeappserver.herokuapp.com/login'),

          body: {
            'email':email,
            'password': password
          }
      );

      if(response.statusCode == 200){
        print('Successfully log in');
        var data = jsonDecode(response.body.toString());
        print(data);

        // saving user name from api response
        var name = data['user']['name'];
        SharedPreferences pref2 = await SharedPreferences.getInstance();
        pref2.setString('name', name ?? '');
        print('myName: $pref2');

        // save login email  in shared preferences
        if (data['isAuth']==true ){

          SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
          sharedPreferences.setString('email', _emailTextController.text);
          print('myEmail: $sharedPreferences');

          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const RecipeListScreen()));
          // Navigator.push(context, MaterialPageRoute(builder: (context)=>const RecipeListScreen()
          // )
          // );
          print('Login Successfully');
        } else {
          showActionSnakeBar2(context);
        }

      } else {
        print(response.body.toString());

      }
    } catch(e) {
      print(e.toString());
    }
  }

  void showActionSnakeBar(BuildContext context){
    const snackBar = SnackBar(
      content: Text('Please Enter Email and Password'),
    );

    // Find the ScaffoldMessenger in the widget tree
    // and use it to show a SnackBar.
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
  void showActionSnakeBar2(BuildContext context){
    const snackBar = SnackBar(
      content: Text('Email not found'),
    );

// Find the ScaffoldMessenger in the widget tree
// and use it to show a SnackBar.
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
  showLoaderDialog(BuildContext context){
    AlertDialog alert = AlertDialog(
      content: Row(
        children: [
          const CircularProgressIndicator(),
          Container(margin: const EdgeInsets.only(left: 7),child:const Text("Loading..." )),
        ],),
    );
    showDialog(barrierDismissible: false,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(body: Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      height: MediaQuery
          .of(context)
          .size
          .height,
      decoration: BoxDecoration(gradient: LinearGradient(colors: [
        hexStringToColor("CB2B93"),
        hexStringToColor("9546C4"),
        hexStringToColor("5E61F4")
      ],
          begin: Alignment.topCenter, end: Alignment.bottomCenter
      )
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(20, MediaQuery
              .of(context)
              .size
              .height * 0.2, 20, 0
          ),
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),
                ),
                child: const Text('Welcome to Cook Book',
                  style:  TextStyle(
                      fontSize: 28.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.white
                  ),
                ),

              ),
              const SizedBox(height: 50,
              ),
              resuableTextField('Enter Email', Icons.person_outline, false,
                  _emailTextController),

              const SizedBox(height: 20,
              ),
              resuableTextField('Enter Password', Icons.lock_outline, true,
                  _passwordTextController),
              const SizedBox(height: 20,),
              signInSignUpButton(context, true, () async {

                if(_emailTextController.text.toString().isNotEmpty && _passwordTextController.text.toString().isNotEmpty ){
                  logIn(_emailTextController.text.toString(), _passwordTextController.text.toString());
                } else {
                  showActionSnakeBar(context);
                }

              }),
              googleSignIn(context, true, (){

              }),

              signUpOption(context),
            ],

          ),

        ),

      ),
    ),
    );
  }

  Row signUpOption(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text('Not a Member?',
          style: TextStyle(color: Colors.white70),

        ),
        GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => SignUpScreen()
                )
            );
          },
          child: const Text(
            ' Sign Up',
            style: TextStyle(fontWeight: FontWeight.bold
            ),
          ),
        )
      ],
    );
  }



}
