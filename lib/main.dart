import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_demo/screens/recipe_list_screen.dart';
import 'package:firebase_demo/screens/signin_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var email = sharedPreferences.getString('email');
  await Firebase.initializeApp();
  runApp(MaterialApp(home: email == null ? const SignInScreen(): const RecipeListScreen(),));
}






